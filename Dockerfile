FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build-stage
ENV ASPNETCORE_URLS=http://+:80
WORKDIR /app
COPY . ./
EXPOSE 80
ENTRYPOINT ["dotnet", "./out/sample-api.dll"]